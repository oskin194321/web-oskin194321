<!DOCTYPE html>
<html>
<head>
	<title>Квит</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>	
	<style>
  </style>
</head>
<meta charset="utf-8">
<body>
	<?php 
	session_start();
	if(isset($_SESSION['userid'])){
		$userid=$_SESSION['userid'];
	}
	else if(isset($_COOKIE['token'])){
		$token=$_COOKIE['token'];
	}
	else{
		header("Location: secure.php");
		exit;
	}
	?>	
	<?php include ("template/header.php");?>	
	<div class="container-fluid">
      <div class="row" align="center">
	  <div class="col-md-10 offset-md-1">	
	<?php
	if(isset($_GET['id_payer'])){
		$id_payer=$_GET['id_payer'];
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");
		$stmt=$mysqli->prepare("SELECT name, address, bill, subsidy, square, residents FROM payer WHERE id_payer=?");
		$stmt->bind_param('i', $id_payer);	
		$stmt->execute();
		$stmt->bind_result($name, $address, $bill, $subsidy, $square, $residents);
		
		echo "<br><h3>Редактирование данных</h3><br><br>";
		while ($stmt->fetch()){
			echo "<form action=\"edituser.php\" method=\"post\">
			<input type=\"hidden\" name=\"id_payer\" value=\"$id_payer\" id=\"user\" placeholder=\"$id_payer\"/>
			<div class=\"form-row\">
			<div class=\"col\">
				<label for=\"hotlabel\">ФИО плательщика</label>
				<input type=\"text\" class=\"form-control\" id=\"hotlabel\" value=\"$name\" required name=\"name\">
			</div>
			<div class=\"col\">
				<label for=\"coldlabel\">Лицевой счет</label>
				<input type=\"text\" class=\"form-control\" id=\"coldlabel\" value=\"$bill\" required name=\"bill\">
			</div>
			<div class=\"col\">
				<label for=\"eelabel\">Адрес</label>
				<input type=\"text\" class=\"form-control\" id=\"eelabel\" value=\"$address\" required name=\"address\">
			</div></div><br><br><div class=\"form-row\">
			<div class=\"col\">
				<label for=\"gaslabel\">Льгота</label>
				<input type=\"text\" class=\"form-control\" id=\"gaslabel\" value=\"$subsidy\" required name=\"subsidy\">
			</div>
				<div class=\"col\">
				<label for=\"tkolabel\">Площадь квартиры</label>
				<input type=\"text\" class=\"form-control\" id=\"tkolabel\" value=\"$square\" required name=\"square\">
			</div>
			<div class=\"col\">
				<label for=\"remlabel\">Проживает в квартире</label>
				<input type=\"text\" class=\"form-control\" id=\"remlabel\" value=\"$residents\" required name=\"residents\">
			</div>
			<div class=\"col\">
				<label for=\"oklabel\">Внести изменения</label>
				<input type=\"submit\" value=\"ОК\" class=\"btn btn-block btn-primary\" id=\"oklabel\"></div>
			</div>
			</div>
			</form>";
		}		
		echo "<br>";
	}	
	?>
	</div><br><br><br>
	<div class="row" align="center">
	<div class="col-md-2 offset-md-5">
	<input type="button" class="btn btn-block btn-warning" onclick="history.back();" value="Вернуться на главную"/>
	</div>
	</div>
	</div>
   </div>
</body>
</html>