<!DOCTYPE html>
<html lang="ru">
<head>
	<title>Квит</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>	
	<div class="container">
		<div class="row">
			<div class="col-5">
				<div class="limiter">
					<div class="container-login100">
						<div class="wrap-login100 p-t-75 p-b-90">			
					
							<span class="login100-form-title p-b">
								Справочная
																				
							<?php
							$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
							$mysqli->query("SET NAMES 'utf8';");
							$stmt=$mysqli->prepare("SELECT receiver, worktime, callnumber, elecnumber, sannumber, avarnumber FROM tariffs");	
							$stmt->execute();
							$stmt->bind_result($receiver, $worktime, $callnumber, $elecnumber, $sannumber, $avarnumber);
							while ($stmt->fetch()){
								echo "<br><br><p class=\"font-weight-bold\">
								Режим работы: $worktime<br><br>
								Телефон: $callnumber<br><br>
								Телефон электрика: $elecnumber<br><br>
								Телефон сантехника: $sannumber<br><br>
								Аварийная служба: $avarnumber<br><br>
								Служба поддержки: 8-964-877-87-77<br><br></p>";
							}		
							
							$mysqli-> close();
							?>
							</span>	
						</div>
					</div>
				</div>
			</div>
			<div class="col">		
				<div class="limiter">
					<div class="container-login100">
						<div class="wrap-login100 p-t-50 p-b-90">
			
							<form action="secure.php" method="post" class="login100-form validate-form flex-sb flex-w">
					
							<span class="login100-form-title p-b-51">
								Вход в систему квит
							</span>
							
							<div class="wrap-input100 validate-input m-b-16" data-validate = "Требуется ввести логин">
								<input class="input100" type="text" name="login" placeholder="Логин">
								<span class="focus-input100"></span>
							</div>
							
							<div class="wrap-input100 validate-input m-b-16" data-validate = "Требуется ввести пароль">
								<input class="input100" type="password" name="pwd" placeholder="Пароль">
								<span class="focus-input100"></span>
							</div>

							<div class="container-login100-form-btn m-t-17">
								<button type="submit" class="login100-form-btn">
									Войти
								</button>
							</div>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>