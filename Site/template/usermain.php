<div class="container-fluid">
      <div class="row" align="left">
		 <div class="col-md-3 offset-md-1"><br>	 
		 
		<?php
		$status=$_SESSION['status'];
		 
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");
		$stmt=$mysqli->prepare("SELECT id_payer, name, address, bill, square, residents FROM payer WHERE id_user=?");
		$stmt->bind_param('i', $_SESSION['userid']);	
		$stmt->execute();
		$stmt->bind_result($id_payer, $name, $address, $bill, $square, $residents);
		while ($stmt->fetch()){
			$_SESSION['id_payer']=$id_payer;
			echo "<h4 align=\"center\">Информация о плательщике</h4><br><p class=\"font-weight-normal\">ФИО: $name<br><br>
			Адрес: $address<br><br>
			Лицевой счет: $bill<br><br>
			Площадь: $square м²<br><br>
			Проживает: $residents чел</p><br>";
		}		
		echo "<h4 align=\"center\">Управляющая компания</h4><br>";
		
		$stmt=$mysqli->prepare("SELECT receiver, worktime, callnumber, elecnumber, sannumber, avarnumber FROM tariffs");	
		$stmt->execute();
		$stmt->bind_result($receiver, $worktime, $callnumber, $elecnumber, $sannumber, $avarnumber);
		while ($stmt->fetch()){
			echo "<p class=\"font-weight-normal\">Название: $receiver<br><br>
			Режим работы: $worktime<br><br>
			Телефон: $callnumber<br><br>
			Телефон электрика: $elecnumber<br><br>
			Телефон сантехника: $sannumber<br><br>
			Аварийная служба: $avarnumber<br><br></p>";
		}		
		
		$mysqli-> close();
		?>
		 </div>
		 <div class="col-md-1"></div>
         <div class="col-md-7" align="left"><br>
		 
		 <?php
		 if($status==1){
			 echo "<h4 align=\"center\">Начисления</h4><br>";


			 
			 
			 echo "<table class=\"table table-bordered table-hover table-striped table-sm text-center\">
			<thead>
			<tr class=\"table-primary\">
			<th scope=\"col\">Месяц / Год</th>
			<th scope=\"col\">Начислено</th>
			<th scope=\"col\">Долг</th>
			<th scope=\"col\">Пени</th>
			<th scope=\"col\">Всего к оплате</th>
				</tr>
			  </thead>
			  <tbody>";
			$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
			$mysqli->query("SET NAMES 'utf8';");

			$stmt=$mysqli->prepare("SELECT month, year, sum, debt, penalty FROM calculation WHERE id_payer=? ORDER BY year DESC, month DESC");	
			$stmt->bind_param('i', $_SESSION['id_payer']);
			$stmt->execute();
			$stmt->bind_result($month, $year, $sum, $debt, $penalty);
			while ($stmt->fetch()){
				$all=$sum+$debt+$penalty;
				echo "<tr>
				<td>$month / $year</td>
				<td>$sum руб.</td>
				<td>$debt руб.</td>
				<td>$penalty руб.</td>
				<th>$all руб.</th>
				</tr>";
			}	
			$mysqli-> close();
			echo "</tbody>
			</table>";
			
			
			
			
			
			 echo "</div>";
		 }
		 
		 if($status==10){
			echo "<h4 align=\"center\">Плательщики</h4><br>		 

			<table class=\"table table-bordered table-hover table-striped table-sm text-center\">
			<thead>
			<tr class=\"table-primary\">
			<th scope=\"col\">ФИО</th>
			<th scope=\"col\">Лицевой счет</th>
			<th scope=\"col\">Адрес</th>
			<th scope=\"col\">Редактирование</th>
			</tr>
			</thead>
			<tbody>";

			$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
			$mysqli->query("SET NAMES 'utf8';");

			$stmt=$mysqli->prepare("SELECT id_payer, name, address, bill FROM payer");	
			$stmt->execute();
			$stmt->bind_result($a_payer, $a_name, $a_address, $a_bill);
			while ($stmt->fetch()){
				echo "<tr>
				<td>$a_name</td>
				<td>$a_bill</td>
				<td>$a_address</td>
				<td><a href=\"updatepayer.php?id_payer=$a_payer\">Изменить</a></td>
				</tr>";
			}	
			$mysqli-> close();

			echo "</tbody>
			</table>		 
			</div>";
			 }
		 ?>
      </div>
   </div>