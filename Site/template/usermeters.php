<div class="container-fluid">
	<div class="row" align="left">
	<div class="col-md-3 offset-md-1"><br>
	<h4 align="center">Ввод новых показаний</h4>
	
	<?php
	$status=$_SESSION['status'];
	
	if($status==10){
		echo "<br><form action=\"billtopayer.php\" method=\"post\">
		<div class=\"form-group row\">
		<label for=\"bill\" class=\"col-sm-5 col-form-label\">Лицевой счет:</label>
		<div class=\"col-sm-5\">
		<select class=\"custom-select\" id=\"bill\" name=\"bill\">";
		if(isset($_SESSION['bill'])){
			$bill=$_SESSION['bill'];
			echo "<option selected value=\"$bill\">$bill</option>";
		}
		$conn=mysqli_connect("localhost", "root", "", "house");
			if ($conn-> connect_error) {
				die("Connection failed:". $conn->connect_error);
			}
			mysqli_set_charset($conn, 'utf8');	
			$sql="SELECT bill FROM payer";
			$result= $conn-> query($sql);	
			if ($result-> num_rows > 0) {
				while ($row= $result-> fetch_assoc()) {
					$newbill=$row["bill"];
					echo "<option value=\"$newbill\">$newbill</option>";
				}
			}
			else {
				echo "0 result";
			}	
			$conn-> close();		
		echo "</select>";		
		echo "</div><button type=\"submit\" class=\"btn btn-primary\">ОК</button></div></form>";
		
		if(isset($_SESSION['billuser'])){
			$userid=$_SESSION['billuser'];
			addnewmeters($userid);
			echo "";
		}
	}

	if($status==1){
		$userid=$_SESSION['userid'];
		addnewmeters($userid);
	}
	
	function addnewmeters($userid){
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");
		$current_year = date ( 'Y' );
		$current_month = date ( 'm' );
		$stmt=$mysqli->prepare("SELECT id_payer FROM payer WHERE id_user=?");
		$stmt->bind_param('i', $userid);	
		$stmt->execute();
		$stmt->bind_result($payerid);
		while ($stmt->fetch()){
			$idpayer=$payerid;
		}
		
		$stmt=$mysqli->prepare("SELECT hot, cold, elec, gas FROM meters WHERE month in (SELECT max(month) FROM meters WHERE year=? AND id_payer=?) AND id_payer=? AND year=?");
		$stmt->bind_param('iiii', $current_year, $idpayer, $idpayer, $current_year);	
		$stmt->execute();
		$stmt->bind_result($hot, $cold, $elec, $gas);
		while ($stmt->fetch()){
			echo "<form action=\"addnewmeters.php\" method=\"post\">	
			<input type=\"hidden\" name=\"idpayer\" value=\"$idpayer\"/>
			<div align=\"center\">Укажите месяц / год</div>
			<div class=\"form-group row\">
			<div class=\"col-sm-6\">
			<select class=\"custom-select\" id=\"hot\" name=\"month\">
			<option value=\"1\">01 - Январь</option>";
			if($current_month>1){
				echo "<option value=\"2\">02 - Февраль</option>";
			}
			if($current_month>2){
				echo "<option value=\"3\">03 - Март</option>";
			}
			if($current_month>3){
				echo "<option value=\"4\">04 - Апрель</option>";
			}
			if($current_month>4){
				echo "<option value=\"5\">05 - Май</option>";
			}
			if($current_month>5){
				echo "<option value=\"6\">06 - Июнь</option>";
			}
			if($current_month>6){
				echo "<option value=\"7\">07 - Июль</option>";
			}
			if($current_month>7){
				echo "<option value=\"8\">08 - Август</option>";
			}
			if($current_month>8){
				echo "<option value=\"9\">09 - Сентябрь</option>";
			}
			if($current_month>9){
				echo "<option value=\"10\">10 - Октябрь</option>";
			}
			if($current_month>10){
				echo "<option value=\"11\">11 - Ноябрь</option>";
			}
			if($current_month>11){
				echo "<option value=\"12\">12 - Декабрь</option>";
			}			
			echo "</select></div>
			<div class=\"col-sm-6\"><select class=\"custom-select\" id=\"hot\" name=\"year\">
			<option value=\"2019\">2019</option>
			</select>
			</div></div>			
			<div><b>ГВС</b> (предыдущее показание: $hot)</div><br>
			<div class=\"form-group row\">
			<label for=\"hot\" class=\"col-sm-6 col-form-label\">Новое показание</label>
			<div class=\"col-sm-6\">
			<input type=\"number\" class=\"form-control\" id=\"hot\" min=\"$hot\" required name=\"hot\"/>
			</div></div>
			<div><b>ХВС</b> (предыдущее показание: $cold)</div><br>
			<div class=\"form-group row\">
			<label for=\"hot\" class=\"col-sm-6 col-form-label\">Новое показание</label>
			<div class=\"col-sm-6\">
			<input type=\"number\" class=\"form-control\" id=\"hot\" min=\"$cold\" required name=\"cold\"/>
			</div></div>
			<div><b>Э/э</b> (предыдущее показание: $elec)</div><br>
			<div class=\"form-group row\">
			<label for=\"hot\" class=\"col-sm-6 col-form-label\">Новое показание</label>
			<div class=\"col-sm-6\">
			<input type=\"number\" class=\"form-control\" id=\"hot\" min=\"$elec\" required name=\"elec\"/>
			</div></div>
			<div><b>Газ</b> (предыдущее показание: $gas)</div><br>
			<div class=\"form-group row\">
			<label for=\"hot\" class=\"col-sm-6 col-form-label\">Новое показание</label>
			<div class=\"col-sm-6\">
			<input type=\"number\" class=\"form-control\" id=\"hot\" min=\"$gas\" required name=\"gas\"/>
			</div></div>
			<div class=\"text-center\"><input type=\"reset\" value=\"Сбросить\" class=\"btn btn-sm btn-warning\">
			<a href=\"deletemeters.php?id_payer=$idpayer\" class=\"btn btn-sm btn-danger\" role=\"button\" aria-pressed=\"true\">Удалить показание</a>
			<input type=\"submit\" value=\"Добавить\" class=\"btn btn-sm btn-primary\"/></div>
			</form>";
		}		
		$mysqli-> close();
	}
	?>
	 </div>
	 <div class="col-md-1"></div>
	 <div class="col-md-7" align="left"><br>
	 <h4 align="center">Показания приборов учета</h4><br>		 
	 
	 <table class="table table-bordered table-hover table-striped table-sm text-center">
	  <thead>
		<tr class="table-primary">
		  <th scope="col">Месяц / год</th>
		  <th scope="col">ГВС</th>
		  <th scope="col">ХВС</th>
		  <th scope="col">Электроэнергия</th>
		  <th scope="col">Газ</th>
		</tr>
	  </thead>
	  <tbody>
		<?php 
		if($status==1)
			showmeters($userid);
		
		if($status==10){
			if(isset($_SESSION['billuser'])){
				$userid=$_SESSION['billuser'];
				showmeters($userid);
			}
		}		
	
		function showmeters($userid){
			$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
			$mysqli->query("SET NAMES 'utf8';");
			$stmt=$mysqli->prepare("SELECT id_payer FROM payer WHERE id_user=?");
			$stmt->bind_param('i', $userid);	
			$stmt->execute();
			$stmt->bind_result($payerid);
			while ($stmt->fetch()){
				$idpayer=$payerid;
			}
			
			$stmt=$mysqli->prepare("SELECT month, year, hot, cold, elec, gas FROM meters WHERE id_payer=? ORDER BY year DESC, month DESC");
			$stmt->bind_param('i', $idpayer);	
			$stmt->execute();
			$stmt->bind_result($month, $year, $hot, $cold, $elec, $gas);
			$onlyyear=0;
			while ($stmt->fetch()){
				$onlyyear++;
				if($onlyyear<16)
				echo "<tr>
			  <td>$month / $year</td>
			  <td>$hot</td>
			  <td>$cold</td>
			  <td>$elec</td>
			  <td>$gas</td>
			</tr>";
			}	
			$mysqli-> close();
		}
		?>
	  </tbody>
	</table>		 
	 
	 </div>
  </div>
</div> 
   
<?php
if(isset($_GET['do'])){
	if($_GET['do'] == 'alert'){
		echo "<script>
		alert(\"Показания приборов учета за указанный месяц уже добавлены\");
		</script>";
	}
}
?>