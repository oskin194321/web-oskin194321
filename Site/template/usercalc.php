<div class="container-fluid">
	<div class="col-md-10 offset-md-1"><br>
	 <h4>Коммунальные услуги</h4>		 
		<?php 
		$status=$_SESSION['status'];
		
		if($status==1){
			echo "<br><table class=\"table table-bordered table-hover table-striped table-sm text-center\">
			<thead>
			<tr class=\"table-primary\">
			<th scope=\"col\">Месяц / Год</th>
			<td scope=\"col\">ГВС<br>куб.м</td>
			<td scope=\"col\">ГВС<br>руб.</td>
			<td scope=\"col\">ХВС<br>куб.м</td>
			<td scope=\"col\">ХВС<br>руб.</td>
			<td scope=\"col\">Э/э<br>кВт.ч</td>
			<td scope=\"col\">Э/э<br>руб.</td>
			<td scope=\"col\">Газ<br>куб.м</td>
			<td scope=\"col\">Газ<br>руб.</td>
			<td scope=\"col\">ТКО<br>руб.</td>
			<th scope=\"col\">Начислено<br>руб.</th>
			<td scope=\"col\">Капремонт<br>руб.</td>
			<th scope=\"col\">Всего<br>руб.</th>
			<th scope=\"col\"></th>
				</tr>
			  </thead>
			  <tbody>";
			$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
			$mysqli->query("SET NAMES 'utf8';");

			$stmt=$mysqli->prepare("SELECT month, year, chot, hotcost, ccold, coldcost, celec, eleccost, cgas, gascost, ctko, cremont, sum FROM calculation WHERE id_payer=? ORDER BY year DESC, month DESC");	
			$stmt->bind_param('i', $_SESSION['id_payer']);
			$stmt->execute();
			$stmt->bind_result($month, $year, $chot, $hotcost, $ccold, $coldcost, $celec, $eleccost, $cgas, $gascost, $ctko, $cremont, $sum);
			while ($stmt->fetch()){
				$allsum=$sum-$cremont;
				echo "<tr>
				<td>$month / $year</td>
				<td>$chot</td>
				<td>$hotcost</td>
				<td>$ccold</td>
				<td>$coldcost</td>
				<td>$celec</td>
				<td>$eleccost</td>
				<td>$cgas</td>
				<td>$gascost</td>
				<td>$ctko</td>
				<th>$allsum</th>
				<td>$cremont</td>
				<th>$sum</th>
				<td class=\"table-light\"><a href=\"createpdf.php?month=$month&year=$year\" target=\"_blank\" class=\"btn btn-sm btn-success\" role=\"button\" aria-pressed=\"true\">квит</a></td>
				</tr>";
			}	
			$mysqli-> close();
			echo "</tbody>
			</table><br>";
		}
		
		if($status==10){
			
			echo "<br><form action=\"calcmonthyear.php\" method=\"post\">
			<div>Выберите месяц / год</div><br>
			<div class=\"form-group row\">
			
			<div class=\"col-sm-2\">
			<select class=\"custom-select\" id=\"month\" name=\"month\">";
			if(isset($_SESSION['calcmonth'])){
				$calcmonth=$_SESSION['calcmonth'];
				echo "<option selected value=\"$calcmonth\">$calcmonth</option>";
			}
			echo "<option value=\"1\">01 - Январь</option>
			<option value=\"2\">02 - Февраль</option>
			<option value=\"3\">03 - Март</option>
			<option value=\"4\">04 - Апрель</option>
			<option value=\"5\">05 - Май</option>
			<option value=\"6\">06 - Июнь</option>
			<option value=\"7\">07 - Июль</option>
			<option value=\"8\">08 - Август</option>
			<option value=\"9\">09 - Сентябрь</option>
			<option value=\"10\">10 - Октябрь</option>
			<option value=\"11\">11 - Ноябрь</option>
			<option value=\"12\">12 - Декабрь</option>
			</select></div>";	
			
			echo "<div class=\"col-sm-2\">
			<select class=\"custom-select\" id=\"month\" name=\"year\">";
			if(isset($_SESSION['calcyear'])){
				$calcyear=$_SESSION['calcyear'];
				echo "<option selected value=\"$calcyear\">$calcyear</option>";
			}
			echo "<option value=\"2019\">2019</option>
			<option value=\"2018\">2018</option>
			</select></div>";			
			echo "<div class=\"col-sm-1\"><button type=\"submit\" class=\"btn btn-primary\">ОК</button></div>";
			
			if(isset($_SESSION['calcmonth'])){
				echo "</form>
				<div class=\"col-sm-2\"><a href=\"calculatemonth.php?calcmonth=$calcmonth&calcyear=$calcyear\" 
				class=\"btn btn-success\" role=\"button\" aria-pressed=\"true\">Расчет за $calcmonth/$calcyear</a></div>
				<div class=\"col-sm-2\">
				<a href=\"createpdf.php\" target=\"_blank\" class=\"btn btn-success\" role=\"button\" aria-pressed=\"true\">Квитанции за $calcmonth/$calcyear</a></div>";
				
				$current_year = date ( 'Y' );
				$current_month = date ( 'm' );
				if($current_month-1>$calcmonth || $current_year>$calcyear)
					echo "<div class=\"col-sm-2\"><a href=\"calcpenalty.php?calcmonth=$calcmonth&calcyear=$calcyear\" 
					class=\"btn btn-warning\" role=\"button\" aria-pressed=\"true\">Пени за $calcmonth/$calcyear</a></div>";
				
				
				echo "</div>";
			}
			else			
				echo "</form></div>";
			echo "<br>";
			
			if(isset($_SESSION['calcmonth'])&&isset($_SESSION['calcyear'])){
				echo "<table class=\"table table-bordered table-hover table-striped table-sm text-center\">
				<thead>
				<tr class=\"table-primary\">
				<th scope=\"col\">Лицевой<br>счет</th>
				<td scope=\"col\">ГВС<br>куб.м / руб.</td>
				<td scope=\"col\">ХВС<br>куб.м / руб.</td>
				<td scope=\"col\">Э/э<br>кВт.ч / руб.</td>
				<td scope=\"col\">Газ<br>куб.м / руб.</td>
				<td scope=\"col\">ТКО<br>руб.</td>
				<th scope=\"col\">Начислено<br>руб.</th>
				<td scope=\"col\">Капремонт<br>руб.</td>
				<th scope=\"col\">Всего<br>руб.</th>
				<th scope=\"col\">Долг<br>руб.</th>
				<th scope=\"col\">Пени<br>руб.</th>
				</tr>
				</thead>
				<tbody>";
				$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
				$mysqli->query("SET NAMES 'utf8';");

				$stmt=$mysqli->prepare("SELECT cbill, chot, hotcost, ccold, coldcost, celec, eleccost, cgas, gascost, ctko, cremont, sum, debt, penalty FROM calculation WHERE month=? AND year=?");	
				$stmt->bind_param('ii', $_SESSION['calcmonth'], $_SESSION['calcyear']);
				$stmt->execute();
				$stmt->bind_result($cbill, $chot, $hotcost, $ccold, $coldcost, $celec, $eleccost, $cgas, $gascost, $ctko, $cremont, $sum, $debt, $penalty);
				while ($stmt->fetch()){
					$allsum=$sum-$cremont;
					echo "<tr>
					<th>$cbill</th>
					<td>$chot / $hotcost</td>
					<td>$ccold / $coldcost</td>
					<td>$celec / $eleccost</td>
					<td>$cgas / $gascost</td>
					<td>$ctko</td>
					<th>$allsum</th>
					<td>$cremont</td>
					<th>$sum</th>
					<td width=\"14%\"><form  action=\"calcdebt.php\" method=\"post\">
					<input type=\"hidden\" name=\"bill\" value=\"$cbill\">
					<div class=\"form-row\">
					
						<div class=\"col\">
							<input type=\"text\" class=\"form-control form-control-sm\" value=\"$debt\" required name=\"debt\">
						</div>
						<div class=\"col\">
							<input type=\"submit\" value=\"Сохр.\" class=\"btn btn-sm btn-block btn-info\"></div>
						</div>
					</div>
					</form></td>
					<td width=\"14%\"><form  action=\"calcdebt.php\" method=\"post\">
					<input type=\"hidden\" name=\"bill\" value=\"$cbill\">
					<div class=\"form-row\">
					
						<div class=\"col\">
							<input type=\"text\" class=\"form-control form-control-sm\" value=\"$penalty\" required name=\"penalty\">
						</div>
						<div class=\"col\">
							<input type=\"submit\" value=\"Сохр.\" class=\"btn btn-sm btn-block btn-info\"></div>
						</div>
					</div>
					</form></td>
					</tr>";
				}	
				$mysqli-> close();
				echo "</tbody>
				</table><br>";
			}
		}
		?>
	</div>
	
	<?php	
	if($status==10){
			
	}	
	?>	
	<hr style="border-width: 1px;">
</div> 