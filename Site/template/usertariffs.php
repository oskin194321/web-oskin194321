<div class="container-fluid">
	<div class="col-md-10 offset-md-1"><br>
	 <h4>Тарифы</h4><br>		 
	 <table class="table table-bordered table-hover table-striped table-sm text-center">
	  <thead>
		<tr class="table-primary">
		  <th scope="col">Горячая вода<br>(куб.м)</th>
		  <th scope="col">Холодная вода<br>(куб.м)</th>
		  <th scope="col">Электроэнергия<br>(кВт.ч)</th>
		  <th scope="col">Газоснабжение<br>(куб.м)</th>
		  <th scope="col">Обращение с ТКО<br>(кв.м)</th>
		  <th scope="col">Капитальный ремонт<br>(кв.м)</th>
		</tr>
	  </thead>
	  <tbody>
		<?php 
		$status=$_SESSION['status'];
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");

		$stmt=$mysqli->prepare("SELECT thot, tcold, telec, tgas, tko, remont FROM tariffs");	
		$stmt->execute();
		$stmt->bind_result($thot, $tcold, $telec, $tgas, $tko, $remont);
		while ($stmt->fetch()){
			echo "<tr>
			<td>$thot руб.</td>
			<td>$tcold руб.</td>
			<td>$telec руб.</td>
			<td>$tgas руб.</td>
			<td>$tko руб.</td>
			<td>$remont руб.</td>
			</tr>";
		}	
		$mysqli-> close();
		?>
	  </tbody>
	</table><br>
	</div>
	
	<?php	
	if($status==10){
		echo "<div class=\"col-md-10 offset-md-1\"><br>
		<form  action=\"updatetariffs.php\" method=\"post\">
		<div class=\"form-row\">
			<div class=\"col\">
				<label for=\"hotlabel\">ГВС</label>
				<input type=\"text\" class=\"form-control\" id=\"hotlabel\" value=\"$thot\" required name=\"thot\">
			</div>
			<div class=\"col\">
				<label for=\"coldlabel\">ХВС</label>
				<input type=\"text\" class=\"form-control\" id=\"coldlabel\" value=\"$tcold\" required name=\"tcold\">
			</div>
			<div class=\"col\">
				<label for=\"eelabel\">Э/э</label>
				<input type=\"text\" class=\"form-control\" id=\"eelabel\" value=\"$telec\" required name=\"telec\">
			</div>
			<div class=\"col\">
				<label for=\"gaslabel\">Газ</label>
				<input type=\"text\" class=\"form-control\" id=\"gaslabel\" value=\"$tgas\" required name=\"tgas\">
			</div>
				<div class=\"col\">
				<label for=\"tkolabel\">ТКО</label>
				<input type=\"text\" class=\"form-control\" id=\"tkolabel\" value=\"$tko\" required name=\"tko\">
			</div>
			<div class=\"col\">
				<label for=\"remlabel\">Кап. ремонт</label>
				<input type=\"text\" class=\"form-control\" id=\"remlabel\" value=\"$remont\" required name=\"remont\">
			</div>
			<div class=\"col\">
				<label for=\"oklabel\">Внести изменения</label>
				<input type=\"submit\" value=\"ОК\" class=\"btn btn-block btn-primary\" id=\"oklabel\"></div>
			</div>
		</div>
		</form>
		<br>
		</div>";		
	}	
	?>	
	<hr style="border-width: 1px;">
	<div class="col-md-10 offset-md-1"><br>
	 <h4>Банковские реквизиты</h4><br>		 
	 <table class="table table-bordered table-hover table-striped table-sm text-center">
	  <thead>
		<tr class="table-primary">
		  <th scope="col">ИНН</th>
		  <th scope="col">КПП</th>
		  <th scope="col">Банк</th>
		  <th scope="col">Корреспондентский счет</th>
		  <th scope="col">БИК</th>
		  <th scope="col">Расчетный счет</th>
		  <th scope="col">Единый лицевой счет</th>
		</tr>
	  </thead>
	  <tbody>
		<?php 
		$status=$_SESSION['status'];
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");

		$stmt=$mysqli->prepare("SELECT inn, kpp, bank, corraccount, bik, checkaccount, unifiedaccount FROM tariffs");	
		$stmt->execute();
		$stmt->bind_result($inn, $kpp, $bank, $corraccount, $bik, $checkaccount, $unifiedaccount);
		while ($stmt->fetch()){
			echo "<tr>
			<td>$inn</td>
			<td>$kpp</td>
			<td>$bank</td>
			<td>$corraccount</td>
			<td>$bik</td>
			<td>$checkaccount</td>
			<td>$unifiedaccount</td>
			</tr>";
		}	

		$mysqli-> close();
		?>
	  </tbody>
	</table><br>	 
	</div>
	
	<?php	
	if($status==10){
		echo "<div class=\"col-md-10 offset-md-1\"><br>
		<form  action=\"updatetariffs.php\" method=\"post\">
		<div class=\"form-row\">
			<div class=\"col\">
				<label for=\"hotlabel\">ИНН</label>
				<input type=\"text\" class=\"form-control\" id=\"hotlabel\" value=\"$inn\" required name=\"inn\">
			</div>
			<div class=\"col\">
				<label for=\"coldlabel\">КПП</label>
				<input type=\"text\" class=\"form-control\" id=\"coldlabel\" value=\"$kpp\" required name=\"kpp\">
			</div>
			<div class=\"col\">
				<label for=\"eelabel\">Банк</label>
				<input type=\"text\" class=\"form-control\" id=\"eelabel\" value=\"$bank\" required name=\"bank\">
			</div>
			<div class=\"col\">
				<label for=\"gaslabel\">Корреспондентский счет</label>
				<input type=\"text\" class=\"form-control\" id=\"gaslabel\" value=\"$corraccount\" required name=\"corraccount\">
			</div>
			</div>
			<br><br><div class=\"form-row\">
				<div class=\"col\">
				<label for=\"tkolabel\">БИК</label>
				<input type=\"text\" class=\"form-control\" id=\"tkolabel\" value=\"$bik\" required name=\"bik\">
			</div>
			<div class=\"col\">
				<label for=\"remlabel\">Расчетный счет</label>
				<input type=\"text\" class=\"form-control\" id=\"remlabel\" value=\"$checkaccount\" required name=\"checkaccount\">
			</div>
			<div class=\"col\">
				<label for=\"remlabel\">Единый лицевой счет</label>
				<input type=\"text\" class=\"form-control\" id=\"remlabel\" value=\"$unifiedaccount\" required name=\"unifiedaccount\">
			</div>
			<div class=\"col\">
				<label for=\"oklabel\">Внести изменения</label>
				<input type=\"submit\" value=\"ОК\" class=\"btn btn-block btn-primary\" id=\"oklabel\"></div>
			</div>
		</div>
		</form><br>
		</div>";		
	}	
	?>	
	<hr style="border-width: 1px;">
	<div class="col-md-10 offset-md-1"><br>
	 <h4>Справочная информация</h4><br>		 
	 <table class="table table-bordered table-hover table-striped table-sm text-center">
	  <thead>
		<tr class="table-primary">
		  <th scope="col">Название</th>
		  <th scope="col">Режим работы</th>
		  <th scope="col">Телефон</th>
		  <th scope="col">Телефон электрика</th>
		  <th scope="col">Телефон сантехника</th>
		  <th scope="col">Аварийная служба</th>
		</tr>
	  </thead>
	  <tbody>
		<?php 
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");

		$stmt=$mysqli->prepare("SELECT receiver, worktime, callnumber, elecnumber, sannumber, avarnumber FROM tariffs");	
		$stmt->execute();
		$stmt->bind_result($receiver, $worktime, $callnumber, $elecnumber, $sannumber, $avarnumber);
		while ($stmt->fetch()){
			echo "<tr>
			<td>$receiver</td>
			<td>$worktime</td>
			<td>$callnumber</td>
			<td>$elecnumber</td>
			<td>$sannumber</td>
			<td>$avarnumber</td>
			</tr>";
		}	

		$mysqli-> close();
		?>
	  </tbody>
	</table><br> 
	</div>
	
	<?php	
	if($status==10){
		echo "<div class=\"col-md-10 offset-md-1\"><br>
		<form  action=\"updatetariffs.php\" method=\"post\">
		<div class=\"form-row\">
			<div class=\"col\">
				<label for=\"hotlabel\">Название</label>
				<input type=\"text\" class=\"form-control\" id=\"hotlabel\" value=\"$receiver\" required name=\"receiver\">
			</div>
			<div class=\"col\">
				<label for=\"coldlabel\">Часы работы</label>
				<input type=\"text\" class=\"form-control\" id=\"coldlabel\" value=\"$worktime\" required name=\"worktime\">
			</div>
			<div class=\"col\">
				<label for=\"eelabel\">Телефон</label>
				<input type=\"text\" class=\"form-control\" id=\"eelabel\" value=\"$callnumber\" required name=\"callnumber\">
			</div>
			<div class=\"col\">
				<label for=\"gaslabel\">Телефон электрика</label>
				<input type=\"text\" class=\"form-control\" id=\"gaslabel\" value=\"$elecnumber\" required name=\"elecnumber\">
			</div>
				<div class=\"col\">
				<label for=\"tkolabel\">Телефон сантехника</label>
				<input type=\"text\" class=\"form-control\" id=\"tkolabel\" value=\"$sannumber\" required name=\"sannumber\">
			</div>
			<div class=\"col\">
				<label for=\"remlabel\">Аварийная служба</label>
				<input type=\"text\" class=\"form-control\" id=\"remlabel\" value=\"$avarnumber\" required name=\"avarnumber\">
			</div>
			<div class=\"col\">
				<label for=\"oklabel\">Внести изменения</label>
				<input type=\"submit\" value=\"ОК\" class=\"btn btn-block btn-primary\" id=\"oklabel\"></div>
			</div>
		</div>
		</form>
		</div>";	
	echo "<br><br><br>";		
	}	
	?>	
</div> 