<div class="container-fluid">
	<div class="col-md-10 offset-md-1"><br>
	 <h4>Потребление горячей воды</h4>	

<?php
$status=$_SESSION['status'];	
	
	if($status==1){
		echo "<div class=\"col-md-12 col-xs-12 padding_top_15\">";
		echo "<h5 class=\"clearfix pull-left\">&nbsp;&nbsp;&nbsp;&nbsp;</h5>";
		echo "<div id=\"grafhot\"></div>";	
		echo "</div>";			
	}
	
	if($status==10){			
					
	}		
?>		

<script>
<?php
	if($status==1){

		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");

		$stmt=$mysqli->prepare("SELECT month, year, chot FROM calculation WHERE id_payer=? ORDER BY year ASC, month ASC");	
		$stmt->bind_param('i', $_SESSION['id_payer']);
		$stmt->execute();
		$stmt->bind_result($cmonth, $cyear, $chot);
		$sumhot=0;
		while ($stmt->fetch()){
			$hotmonth[]="$cmonth / $cyear";
			$sumhot=$sumhot+$chot;
		}	
		$num = count($hotmonth);
		
		$hotaverage=$sumhot/$num;
		
		$url2[0]="'json/hotwater.php'";
		echo '$.getJSON('.$url2[0].', function(json){';
		echo 'var month = [];';
		echo 'for (var i = 0; i < json.length; i++) {';
			echo 'month.push(json[i])';
		echo '}';
		echo 'var datasets2 = [{';
			echo 'label: "Месяц",';
			echo 'fillColor: "rgba(250, 128, 114,0.2)",';
			echo 'strokeColor: "rgba(250, 128, 114,1)",';
			echo 'pointColor: "rgba(250, 128, 114,1)",';
			echo 'pointStrokeColor: "#fff",';
			echo 'pointHighlightFill: "#fff",';
			echo 'pointHighlightStroke: "rgba(250, 128, 114,1)",';
			echo 'data: month';
		echo '}];';
		echo 'var lineChartData2 = {';
			echo 'labels: [';
			echo '" ' . $hotmonth[0] . ' "';
			for($i=1;$i<$num;$i++){	
				echo '," ' . $hotmonth[$i] . ' "';
			}
			echo '],';
			echo 'datasets: datasets2';
		echo '};';
		echo 'new Chart(makeCanvas("grafhot")).Line(lineChartData2, params);';
		echo '});';
	}
?>

// -------------- графики и таблицы -----------------------
function makeCanvas(id, opt_width, opt_height) {
	var container = document.getElementById(id);
	container.innerHTML = '';
	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext('2d');
	canvas.width = opt_width || 600;
	canvas.height = opt_height || 77;
	container.appendChild(canvas);
	return ctx;
};

function makeCanvasBarChart(id, opt_width, opt_height) {
	var container = document.getElementById(id);
	container.innerHTML = '';
	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext('2d');
	container.appendChild(canvas);
	return ctx;
};

var params = {
	responsive: true,
	datasetStrokeWidth : 4,
	animationEasing: "easeOutElastic",
	tooltipCornerRadius: 3,
	scaleFontSize: 14,
	tooltipFontFamily: "'Open Sans', sans-serif",
	tooltipFontStyle: "300",
	tooltipTitleFontStyle: "300",
	tooltipTitleFontFamily: "'Open Sans', sans-serif",
	scaleGridLineWidth : 2,
	bezierCurve : false,
	scaleBeginAtZero : true
};

var paramsLineBarChart = {
	responsive: true,
	barShowStroke : false,
	barValueSpacing : 1,
	scaleShowLabels: false
};

</script>   

<?php
$hotav=round($hotaverage,1);
echo "<br><h6>Среднее значение потребления горячей воды - $hotav м³<h6>";

$hotstat=$chot/$hotav;

if($hotstat>1){
	$hotres=round(($hotstat-1)*100,1);
	echo "<h6>Уровень потребления за последний месяц повысился на $hotres% по отношению к среднему значению за весь период учёта<h6>";
}

if($hotstat==1){
	echo "<h6>Уровень потребления за последний месяц соответствует среднему значению<h6>";
}

if($hotstat<1){
	$hotres=round((1-$hotstat)*100,1);
	echo "<h6>Уровень потребления за последний месяц снизился на $hotres% по отношению к среднему значению за весь период учёта<h6>";
}

?>

<br><hr style="border-width: 1px;">
<h4>Потребление холодной воды</h4>		

<?php
	$status=$_SESSION['status'];	
	
	if($status==1){
		echo "<div class=\"col-md-12 col-xs-12 padding_top_15\">";
		echo "<h5 class=\"clearfix pull-left\">&nbsp;&nbsp;&nbsp;&nbsp;</h5>";
		echo "<div id=\"grafcold\"></div>";	
		echo "</div>";			
	}
	
	if($status==10){			
					
	}
?>		

<script>
<?php

if($status==1){

$mysqli=mysqli_connect("localhost", "root", "", "house")
or die('Connection error.');
$mysqli->query("SET NAMES 'utf8';");

$stmt=$mysqli->prepare("SELECT month, year, ccold 
FROM calculation WHERE id_payer=? ORDER BY year ASC, month ASC");	
$stmt->bind_param('i', $_SESSION['id_payer']);
$stmt->execute();
$stmt->bind_result($cmonth, $cyear, $ccold);
$sumcold=0;
while ($stmt->fetch()){
	$month[]="$cmonth / $cyear";
	$sumcold=$sumcold+$ccold;
}	
$num = count($month);

$coldaverage=$sumcold/$num;

$url2[0]="'json/coldwater.php'";
echo '$.getJSON('.$url2[0].', function(json){';
echo 'var month = [];';
echo 'for (var i = 0; i < json.length; i++) {';
	echo 'month.push(json[i])';
echo '}';
echo 'var datasets2 = [{';
	echo 'label: "Месяц",';
	echo 'fillColor: "rgba(48,160,235,0.2)",';
	echo 'strokeColor: "rgba(48,160,235,1)",';
	echo 'pointColor: "rgba(48,160,235,1)",';
	echo 'pointStrokeColor: "#fff",';
	echo 'pointHighlightFill: "#fff",';
	echo 'pointHighlightStroke: "rgba(48,160,235,1)",';
	echo 'data: month';
echo '}];';
echo 'var lineChartData2 = {';
	echo 'labels: [';
	echo '" ' . $month[0] . ' "';
	for($i=1;$i<$num;$i++){	
		echo '," ' . $month[$i] . ' "';
	}
	echo '],';
	echo 'datasets: datasets2';
echo '};';
echo 'new Chart(makeCanvas("grafcold"))
.Line(lineChartData2, params);';
echo '});';
	}
	
	
	
?>
</script>	
	
<?php
$coldav=round($coldaverage,1);
echo "<br><h6>Среднее значение потребления холодной воды - $coldav м³<h6>";

$coldstat=$ccold/$coldav;

if($coldstat>1){
	$coldres=round(($coldstat-1)*100,1);
	echo "<h6>Уровень потребления за последний месяц повысился на $coldres% по отношению к среднему значению за весь период учёта<h6>";
}

if($coldstat==1){
	echo "<h6>Уровень потребления за последний месяц соответствует среднему значению<h6>";
}

if($coldstat<1){
	$coldres=round((1-$coldstat)*100,1);
	echo "<h6>Уровень потребления за последний месяц снизился на $coldres% по отношению к среднему значению за весь период учёта<h6>";
}

?>

<br><hr style="border-width: 1px;">
<h4>Потребление электроэнергии</h4>		

<?php
	$status=$_SESSION['status'];	
	
	if($status==1){
		echo "<div class=\"col-md-12 col-xs-12 padding_top_15\">";
		echo "<h5 class=\"clearfix pull-left\">&nbsp;&nbsp;&nbsp;&nbsp;</h5>";
		echo "<div id=\"grafelec\"></div>";	
		echo "</div>";			
	}
	
	if($status==10){			
					
	}
?>		

<script>
<?php
	if($status==1){

		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");

		$stmt=$mysqli->prepare("SELECT month, year, celec FROM calculation WHERE id_payer=? ORDER BY year ASC, month ASC");	
		$stmt->bind_param('i', $_SESSION['id_payer']);
		$stmt->execute();
		$stmt->bind_result($cmonth, $cyear, $celec);
		$sumelec=0;
		while ($stmt->fetch()){
			$gasmonth[]="$cmonth / $cyear";
			$sumelec=$sumelec+$celec;
		}	
		$num = count($gasmonth);
		
		$elecaverage=$sumelec/$num;
		
		$url2[0]="'json/elec.php'";
		echo '$.getJSON('.$url2[0].', function(json){';
		echo 'var month = [];';
		echo 'for (var i = 0; i < json.length; i++) {';
			echo 'month.push(json[i])';
		echo '}';
		echo 'var datasets2 = [{';
			echo 'label: "Месяц",';
			echo 'fillColor: "rgba(119, 136, 153,0.2)",';
			echo 'strokeColor: "rgba(119, 136, 153,1)",';
			echo 'pointColor: "rgba(119, 136, 153,1)",';
			echo 'pointStrokeColor: "#fff",';
			echo 'pointHighlightFill: "#fff",';
			echo 'pointHighlightStroke: "rgba(119, 136, 153,1)",';
			echo 'data: month';
		echo '}];';
		echo 'var lineChartData2 = {';
			echo 'labels: [';
			echo '" ' . $gasmonth[0] . ' "';
			for($i=1;$i<$num;$i++){	
				echo '," ' . $gasmonth[$i] . ' "';
			}
			echo '],';
			echo 'datasets: datasets2';
		echo '};';
		echo 'new Chart(makeCanvas("grafelec")).Line(lineChartData2, params);';
		echo '});';
	}
?>
</script>	

<?php
$elecav=round($elecaverage,1);
echo "<br><h6>Среднее значение потребления электроэнергии - $elecav кВт.ч<h6>";

$elecstat=$celec/$elecav;

if($elecstat>1){
	$elecres=round(($elecstat-1)*100,1);
	echo "<h6>Уровень потребления за последний месяц повысился на $elecres% по отношению к среднему значению за весь период учёта<h6>";
}

if($elecstat==1){
	echo "<h6>Уровень потребления за последний месяц соответствует среднему значению<h6>";
}

if($elecstat<1){
	$elecres=round((1-$elecstat)*100,1);
	echo "<h6>Уровень потребления за последний месяц снизился на $elecres% по отношению к среднему значению за весь период учёта<h6>";
}

?>

<br><hr style="border-width: 1px;">
<h4>Потребление газа</h4>		
<?php
	$status=$_SESSION['status'];	
	
	if($status==1){
		echo "<div class=\"col-md-12 col-xs-12 padding_top_15\">";
		echo "<h5 class=\"clearfix pull-left\">&nbsp;&nbsp;&nbsp;&nbsp;</h5>";
		echo "<div id=\"grafgas\"></div>";	
		echo "</div>";			
	}
	
	if($status==10){			
					
	}
?>		

<script>
<?php
	if($status==1){

		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");

		$stmt=$mysqli->prepare("SELECT month, year, cgas FROM calculation WHERE id_payer=? ORDER BY year ASC, month ASC");	
		$stmt->bind_param('i', $_SESSION['id_payer']);
		$stmt->execute();
		$stmt->bind_result($cmonth, $cyear, $cgas);
		$sumgas=0;
		while ($stmt->fetch()){
			$elecmonth[]="$cmonth / $cyear";
			$sumgas=$sumgas+$cgas;
		}	
		$num = count($elecmonth);
		
		$gasaverage=$sumgas/$num;
		
		$url2[0]="'json/gas.php'";
		echo '$.getJSON('.$url2[0].', function(json){';
		echo 'var month = [];';
		echo 'for (var i = 0; i < json.length; i++) {';
			echo 'month.push(json[i])';
		echo '}';
		echo 'var datasets2 = [{';
			echo 'label: "Месяц",';
			echo 'fillColor: "rgba(135, 206, 250,0.2)",';
			echo 'strokeColor: "rgba(135, 206, 250,1)",';
			echo 'pointColor: "rgba(135, 206, 250,1)",';
			echo 'pointStrokeColor: "#fff",';
			echo 'pointHighlightFill: "#fff",';
			echo 'pointHighlightStroke: "rgba(135, 206, 250,1)",';
			echo 'data: month';
		echo '}];';
		echo 'var lineChartData2 = {';
			echo 'labels: [';
			echo '" ' . $elecmonth[0] . ' "';
			for($i=1;$i<$num;$i++){	
				echo '," ' . $elecmonth[$i] . ' "';
			}
			echo '],';
			echo 'datasets: datasets2';
		echo '};';
		echo 'new Chart(makeCanvas("grafgas")).Line(lineChartData2, params);';
		echo '});';
	}
?>
</script>

<?php
$gasav=round($gasaverage,1);
echo "<br><h6>Среднее значение потребления газа - $gasav м³<h6>";

$gasstat=$cgas/$gasav;

if($gasstat>1){
	$gasres=round(($gasstat-1)*100,1);
	echo "<h6>Уровень потребления за последний месяц повысился на $gasres% по отношению к среднему значению за весь период учёта<h6>";
}

if($gasstat==1){
	echo "<h6>Уровень потребления за последний месяц соответствует среднему значению<h6>";
}

if($gasstat<1){
	$gasres=round((1-$gasstat)*100,1);
	echo "<h6>Уровень потребления за последний месяц снизился на $gasres% по отношению к среднему значению за весь период учёта<h6>";
}

?>
	
	<br><br>		
	</div>
</div> 