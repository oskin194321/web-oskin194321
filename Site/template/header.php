<nav class="navbar navbar-expand-sm">
		<a class="navbar-brand mb-0 h1" href="index.php">КВИТ</a>
		<div class="navbar-collapse collapse justify-content-start" id="collapsingNavbar">
			<a class="nav-link" href="index.php">Главная</a>
			<a class="nav-link " href="addmeters.php">Показания</a>
			<a class="nav-link " href="calculation.php">Квитанции</a>
			<a class="nav-link " href="tariffs.php">Тарифы</a>
			
			<?php
			if($_SESSION['status']==1)
				echo "<a class=\"nav-link \" href=\"stat.php\">Статистика</a>";
			
			?>
			
			<a class="nav-link " href="aboutsystem.php">О системе</a>
		</div>
		<div class="justify-content-right order-2" id="collapsingNavbar">	
		<?php
		if(isset($_SESSION['userid'])){
			$userid=$_SESSION['userid'];
			$status=$_SESSION['status'];
			
			$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
			$mysqli->query("SET NAMES 'utf8';");
			$stmt=$mysqli->prepare("SELECT name FROM payer WHERE id_user=?");
			$stmt->bind_param('i', $userid);	
			$stmt->execute();
			$stmt->bind_result($name);
			$stmt->store_result();			
			if($stmt->num_rows==1){
				$stmt->fetch();
				$name1=$name;
			}
			else{
				$name1="Сотрудник";
			}
			echo "<span class=\"navbar-text\">$name1</span>";
		}
		?>	
		<a class="btn btn-sm btn-danger" href="logout.php?do=logout" role="button">Выйти</a>
		</div>
	</nav>