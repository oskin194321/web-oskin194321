<!DOCTYPE html>
<html>
<head>
	<title>Квит</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">	

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/Chart.min.js"></script>
	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>	
	<style>
  </style>
</head>
<meta charset="utf-8">
<body>
	<?php 
	session_start();
	if(isset($_SESSION['userid'])){
		$userid=$_SESSION['userid'];
	}
	else if(isset($_COOKIE['token'])){
		$token=$_COOKIE['token'];
	}
	else{
		header("Location: secure.php");
		exit;
	}
	?>	
	<?php include ("template/header.php");?>
	<?php include ("template/userstat.php");?>	
</body>
</html>