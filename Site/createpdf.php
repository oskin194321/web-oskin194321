<?php 
session_start();
//$status=$_SESSION['status'];

$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
$mysqli->query("SET NAMES 'utf8';");
$stmt=$mysqli->prepare("SELECT thot, tcold, telec, tgas, tko, remont, 
receiver, inn, kpp, bank, corraccount, bik, checkaccount, unifiedaccount, worktime, callnumber FROM tariffs");
$stmt->execute();
$stmt->bind_result($tariffhot, $tariffcold, $tariffelec, $tariffgas, $tarifftko, $tariffremont, 
$treceiver, $tinn, $tkpp, $tbank, $tcorraccount, $tbik, $tcheckaccount, $tunifiedaccount, $tworktime, $tcallnumber);
while ($stmt->fetch()){
	$thot=$tariffhot;
	$tcold=$tariffcold;
	$telec=$tariffelec;
	$tgas=$tariffgas;
	$ttko=$tarifftko;
	$tremont=$tariffremont;
	
	$receiver=$treceiver;
	$inn=$tinn;
	$kpp=$tkpp;
	$bank=$tbank;
	$corraccount=$tcorraccount;
	$bik=$tbik;
	$checkaccount=$tcheckaccount;
	$unifiedaccount=$tunifiedaccount;
	$worktime=$tworktime;
	$callnumber=$tcallnumber;
}	

$html = '
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>	
	<style type="text/css">
		* { 
			font-family: arial;
			font-size: 11px;
			line-height: 11px;
		}
		table {
			margin: 0 0 15px 0;
			width: 100%;
			border-collapse: collapse; 
			border-spacing: 0;
		}        
		table td {
			padding: 5px;
		}    
		table th {
			padding: 5px;
			font-weight: bold;
		}				
	   #col1 {
		width: 30%; /* Ширина первой колонки */
		vertical-align: top;
	   }
	   #col2 {
		width: 70%; /* Ширина второй колонки */
		vertical-align: top;
	   }		
	</style>
</head>
<body>';

if(isset($_SESSION['calcmonth'])&&isset($_SESSION['calcyear'])){
	if($_SESSION['calcmonth']==1)
		$kvityear='Январь';
	if($_SESSION['calcmonth']==2)
		$kvityear='Февраль';
	if($_SESSION['calcmonth']==3)
		$kvityear='Март';
	if($_SESSION['calcmonth']==4)
		$kvityear='Апрель';
	if($_SESSION['calcmonth']==5)
		$kvityear='Май';
	if($_SESSION['calcmonth']==6)
		$kvityear='Июнь';
	if($_SESSION['calcmonth']==7)
		$kvityear='Июль';
	if($_SESSION['calcmonth']==8)
		$kvityear='Август';
	if($_SESSION['calcmonth']==9)
		$kvityear='Сентябрь';
	if($_SESSION['calcmonth']==10)
		$kvityear='Октябрь';
	if($_SESSION['calcmonth']==11)
		$kvityear='Ноябрь';
	if($_SESSION['calcmonth']==12)
		$kvityear='Декабрь';
	
	$stmt=$mysqli->prepare("SELECT id_payer, cbill, chot, hotcost, ccold, coldcost, celec, 
	eleccost, cgas, gascost, ctko, cremont, sum, debt, penalty FROM calculation WHERE month=? AND year=?");
	$stmt->bind_param('ii', $_SESSION['calcmonth'], $_SESSION['calcyear']);
	$stmt->execute();
	$stmt->bind_result($id_payer, $bill, $hot, $hotcost, $cold, $coldcost, $elec, $eleccost, $gas, $gascost, $tko, $remont, $sum, $debt, $penalty);
	while ($stmt->fetch()){
		$idpayers[] = "$id_payer";
		$bills[] = "$bill";
		
		$hots[] = "$hot";
		$hotcosts[] = "$hotcost";
		
		$colds[] = "$cold";
		$coldcosts[] = "$coldcost";
		
		$elecs[] = "$elec";
		$eleccosts[] = "$eleccost";
		
		$gass[] = "$gas";
		$gascosts[] = "$gascost";

		$tkos[] = "$tko";
		$remonts[] = "$remont";	
		
		$sum=$sum-$remont;
		$sums[] = "$sum";
		
		$debts[]="$debt";
		$penalties[]="$penalty";
		
		$allsum=$sum+$debt+$penalty;
		$allsums[]="$allsum";
	}
	
	$num = count($idpayers);
	
	for($i=0;$i<$num;$i++){
		
	$stmt=$mysqli->prepare("SELECT name, address, square, residents FROM payer WHERE id_payer=?");
	$stmt->bind_param('i', $idpayers[$i]);
	$stmt->execute();
	$stmt->bind_result($pname, $paddress, $psquare, $presidents);
	while ($stmt->fetch()){
		$name=$pname;
		$address=$paddress;
		$square=$psquare;
		$residents=$presidents;
	}
		
		if($i>0)
			$html .= '<div style="page-break-before: always;"></div>';
		
		$html .= '
		<table width="90%" cellpadding="5" cellspacing="0">
		<tr>
		<td id="col1">
		<div align="center">
		<h2>Лицевой счет № ' . $bills[$i] . '</h2></div>
		Режим работы:<br> ' . $worktime . '<br><br>
		Телефон: ' . $callnumber . '<br><br>
		Вы можете оплатить квитанцию в отделениях Сбербанка, в любых других финансовых учерждениях, в том числе через интернет-банк		
		
		
		</td>
		<td id="col2">
		<div align="center">
		<h1>ЕДИНЫЙ ПЛАТЕЖНЫЙ ДОКУМЕНТ</h1>
		для внесения платы за предоставление коммунальных услуг
		<br><h2>Расчетный период: ' . $kvityear . ' ' . $_SESSION['calcyear'] . '</h2></div><br>
		Адрес помещения: ' . $address . '<br>
		ФИО плательщика: ' . $name . '<br>
		Площадь (кв.м): ' . $square . '<br>
		Проживает (чел): ' . $residents . '<br>
		
		</td>
		</tr>
		</table>		
		

		<div class="line" style="border-top: 1px dashed #808080;"></div>
		
		<table width="100%" cellpadding="5" cellspacing="0">
		<tr>
		<td id="col1"><div align="center"><br>
		Для оплаты в отделении банка<br><br>
		Оплатить до 25 числа<br>текущего месяца<br>
		</div>
		</td>
		
		<td id="col2">
		<div align="center">
		<h2>Коммунальные услуги</h2></div>		

		<table border="1" width="100%">
			<thead>
				<tr align="center" style="background: #ebebeb;">
					<th width="44%">Вид услуг</th>
					<th width="12%">Ед. изм.</th>
					<th width="12%">Тариф</th>
					<th width="14%">Количество</th>
					<th width="18%">Начислено</th>
				</tr>
			</thead>
			<tbody>';
				$what=11;
				$html .= '
				<tr>
					<td align="left">Электроэнергия</td>
					<td align="center">кВт.ч</td>
					<td align="right">' . $telec . '</td>
					<td align="right">' . $elecs[$i] . '</td>
					<td align="right">' . $eleccosts[$i] . '</td>
				</tr>
				<tr>
					<td align="left">ХВС</td>
					<td align="center">куб.м</td>
					<td align="right">' . $tcold . '</td>
					<td align="right">' . $colds[$i] . '</td>
					<td align="right">' . $coldcosts[$i] . '</td>
				</tr>
				<tr>
					<td align="left">ГВС</td>
					<td align="center">куб.м</td>
					<td align="right">' . $thot . '</td>
					<td align="right">' . $hots[$i] . '</td>
					<td align="right">' . $hotcosts[$i] . '</td>
				</tr>
				<tr>
					<td align="left">Газ</td>
					<td align="center">куб.м</td>
					<td align="right">' . $tgas . '</td>
					<td align="right">' . $gass[$i] . '</td>
					<td align="right">' . $gascosts[$i] . '</td>
				</tr>
				<tr>
					<td align="left">Обращение с ТКО</td>
					<td align="center">кв.м</td>
					<td align="right">' . $ttko . '</td>
					<td align="right">' . $square . '</td>
					<td align="right">' . $tkos[$i] . '</td>
				</tr>
				<tr>
					<td align="left" colspan="4">Начислено</td>
					<td align="right">' . $sums[$i] . '</td>
				</tr>
				<tr>
					<td align="left" colspan="4">Долг</td>
					<td align="right">' . $debts[$i] . '</td>
				</tr>
				<tr>
					<td align="left" colspan="4">Пени</td>
					<td align="right">' . $penalties[$i] . '</td>
				</tr>
				<tr>
					<th align="left" colspan="4">Всего к оплате за расчетный период</th>
					<th align="right">' . $allsums[$i] . '</th>
				</tr>';
			

			$html .= '
			</tbody>
			
		</table><br>	
		Получатель: ' . $receiver . '<br>
		ИНН: ' . $inn . '<br>
		КПП: ' . $kpp . '<br>
		Лицевой счет: ' . $bills[$i] . '<br>
		Банк: ' . $bank . '<br>
		Корр. счет: ' . $corraccount . '<br>
		Расчетный счет: ' . $checkaccount . '<br>
		Единый лицевой счет: ' . $unifiedaccount . '<br><br>
			
		</td>	
		</tr>
		</table>
		
		<div class="line" style="border-top: 1px dashed #808080;"></div>
		
		<table width="100%" cellpadding="5" cellspacing="0">
		<tr>
		<td id="col1"><div align="center"><br>
		Для оплаты в отделении банка<br><br>
		Оплатить до 25 числа<br>текущего месяца<br>
		</div>
		</td>
		
		<td id="col2">
		<div align="center">
		<h2>Капитальный ремонт</h2></div>		

		<table border="1" width="100%">
			<thead>
				<tr align="center" style="background: #ebebeb;">
					<th width="44%">Вид услуг</th>
					<th width="12%">Ед. изм.</th>
					<th width="12%">Тариф</th>
					<th width="14%">Количество</th>
					<th width="18%">Начислено</th>
				</tr>
			</thead>
			<tbody>';
				$what=11;
				$html .= '
				<tr>
					<td align="left">Капитальный ремонт</td>
					<td align="center">кв.м</td>
					<td align="right">' . $tremont . '</td>
					<td align="right">' . $square . '</td>
					<td align="right">' . $remonts[$i] . '</td>
				</tr>
				<tr>
					<th align="left" colspan="4">Всего к оплате за расчетный период</th>
					<th align="right">' . $remonts[$i] . '</th>
				</tr>';
			

			$html .= '
			</tbody>
			
		</table><br>	
		Получатель: ' . $receiver . '<br>
		ИНН: ' . $inn . '<br>
		КПП: ' . $kpp . '<br>
		Лицевой счет: ' . $bills[$i] . '<br>
		Банк: ' . $bank . '<br>
		Корр. счет: ' . $corraccount . '<br>
		Расчетный счет: ' . $checkaccount . '<br>
		Единый лицевой счет: ' . $unifiedaccount . '<br><br>
			
		</td>	
		</tr>
		</table>';	
	}	
	$html .= '</body>
	</html>';
}




if(isset($_GET['month'])&&isset($_GET['year'])){	
	$id_payer=$_SESSION['id_payer'];
	$stmt=$mysqli->prepare("SELECT name, bill, address, square, residents FROM payer WHERE id_payer=?");
	$stmt->bind_param('i', $id_payer);
	$stmt->execute();
	$stmt->bind_result($pname, $pbill, $paddress, $psquare, $presidents);
	while ($stmt->fetch()){
		$name=$pname;
		$bill=$pbill;
		$address=$paddress;
		$square=$psquare;
		$residents=$presidents;
	}
	
	$stmt=$mysqli->prepare("SELECT chot, hotcost, ccold, coldcost, celec, eleccost, 
	cgas, gascost, ctko, cremont, sum, debt, penalty FROM calculation WHERE month=? AND year=? AND id_payer=?");
	$stmt->bind_param('iii', $_GET['month'], $_GET['year'], $id_payer);
	$stmt->execute();
	$stmt->bind_result($phot, $photcost, $pcold, $pcoldcost, $pelec, $peleccost, $pgas, $pgascost, $ptko, $premont, $psum, $pdebt, $ppenalty);
	while ($stmt->fetch()){		
		$hot = "$phot";
		$hotcost = "$photcost";		
		$cold = "$pcold";
		$coldcost = "$pcoldcost";		
		$elec = "$pelec";
		$eleccost = "$peleccost";		
		$gas = "$pgas";
		$gascost = "$pgascost";
		$tko = "$ptko";
		$remont = "$premont";		
		$sum = "$psum"-$remont;
		
		$debt="$pdebt";
		$penalty="$ppenalty";
		$allsum=$sum+$debt+$penalty;
	}
	
	if($_GET['month']==1)
		$kvityear='Январь';
	if($_GET['month']==2)
		$kvityear='Февраль';
	if($_GET['month']==3)
		$kvityear='Март';
	if($_GET['month']==4)
		$kvityear='Апрель';
	if($_GET['month']==5)
		$kvityear='Май';
	if($_GET['month']==6)
		$kvityear='Июнь';
	if($_GET['month']==7)
		$kvityear='Июль';
	if($_GET['month']==8)
		$kvityear='Август';
	if($_GET['month']==9)
		$kvityear='Сентябрь';
	if($_GET['month']==10)
		$kvityear='Октябрь';
	if($_GET['month']==11)
		$kvityear='Ноябрь';
	if($_GET['month']==12)
		$kvityear='Декабрь';
	$html .= '
		<table width="90%" cellpadding="5" cellspacing="0">
		<tr>
		<td id="col1">
		<div align="center">
		<h2>Лицевой счет № ' . $bill . '</h2></div>
		Режим работы:<br> ' . $worktime . '<br><br>
		Телефон: ' . $callnumber . '<br><br>
		Вы можете оплатить квитанцию в отделениях Сбербанка, в любых других финансовых учерждениях, в том числе через интернет-банк		
		
		
		</td>
		<td id="col2">
		<div align="center">
		<h1>ЕДИНЫЙ ПЛАТЕЖНЫЙ ДОКУМЕНТ</h1>
		для внесения платы за предоставление коммунальных услуг
		<br><h2>Расчетный период: ' . $kvityear . ' ' . $_GET['year'] . '</h2></div><br>
		Адрес помещения: ' . $address . '<br>
		ФИО плательщика: ' . $name . '<br>
		Площадь (кв.м): ' . $square . '<br>
		Проживает (чел): ' . $residents . '<br>
		
		</td>
		</tr>
		</table>		
		

		<div class="line" style="border-top: 1px dashed #808080;"></div>
		
		<table width="100%" cellpadding="5" cellspacing="0">
		<tr>
		<td id="col1"><div align="center"><br>
		Для оплаты в отделении банка<br><br>
		Оплатить до 25 числа<br>текущего месяца<br>
		</div>
		</td>
		
		<td id="col2">
		<div align="center">
		<h2>Коммунальные услуги</h2></div>
		

		<table border="1" width="100%">
			<thead>
				<tr align="center" style="background: #ebebeb;">
					<th width="44%">Вид услуг</th>
					<th width="12%">Ед. изм.</th>
					<th width="12%">Тариф</th>
					<th width="14%">Количество</th>
					<th width="18%">Начислено</th>
				</tr>
			</thead>
			<tbody>';
				$what=11;
				$html .= '
				<tr>
					<td align="left">Электроэнергия</td>
					<td align="center">кВт.ч</td>
					<td align="right">' . $telec . '</td>
					<td align="right">' . $elec . '</td>
					<td align="right">' . $eleccost . '</td>
				</tr>
				<tr>
					<td align="left">ХВС</td>
					<td align="center">куб.м</td>
					<td align="right">' . $tcold . '</td>
					<td align="right">' . $cold . '</td>
					<td align="right">' . $coldcost . '</td>
				</tr>
				<tr>
					<td align="left">ГВС</td>
					<td align="center">куб.м</td>
					<td align="right">' . $thot . '</td>
					<td align="right">' . $hot . '</td>
					<td align="right">' . $hotcost . '</td>
				</tr>
				<tr>
					<td align="left">Газ</td>
					<td align="center">куб.м</td>
					<td align="right">' . $tgas . '</td>
					<td align="right">' . $gas . '</td>
					<td align="right">' . $gascost . '</td>
				</tr>
				<tr>
					<td align="left">Обращение с ТКО</td>
					<td align="center">кв.м</td>
					<td align="right">' . $ttko . '</td>
					<td align="right">' . $square . '</td>
					<td align="right">' . $tko . '</td>
				</tr>
				<tr>
					<td align="left" colspan="4">Начислено</td>
					<td align="right">' . $sum . '</td>
				</tr>
				<tr>
					<td align="left" colspan="4">Долг</td>
					<td align="right">' . $debt . '</td>
				</tr>
				<tr>
					<td align="left" colspan="4">Пени</td>
					<td align="right">' . $penalty . '</td>
				</tr>
				<tr>
					<th align="left" colspan="4">Всего к оплате за расчетный период</th>
					<th align="right">' . $allsum . '</th>
				</tr>';
			

			$html .= '
			</tbody>
			
		</table><br>	
		Получатель: ' . $receiver . '<br>
		ИНН: ' . $inn . '<br>
		КПП: ' . $kpp . '<br>
		Лицевой счет: ' . $bill . '<br>
		Банк: ' . $bank . '<br>
		Корр. счет: ' . $corraccount . '<br>
		Расчетный счет: ' . $checkaccount . '<br>
		Единый лицевой счет: ' . $unifiedaccount . '<br><br>
			
		</td>	
		</tr>
		</table>
		
		<div class="line" style="border-top: 1px dashed #808080;"></div>
		
		<table width="100%" cellpadding="5" cellspacing="0">
		<tr>
		<td id="col1"><div align="center"><br>
		Для оплаты в отделении банка<br><br>
		Оплатить до 25 числа<br>текущего месяца<br>
		</div>
		</td>
		
		<td id="col2">
		<div align="center">
		<h2>Капитальный ремонт</h2></div>
		

		<table border="1" width="100%">
			<thead>
				<tr align="center" style="background: #ebebeb;">
					<th width="44%">Вид услуг</th>
					<th width="12%">Ед. изм.</th>
					<th width="12%">Тариф</th>
					<th width="14%">Количество</th>
					<th width="18%">Начислено</th>
				</tr>
			</thead>
			<tbody>';
				$what=11;
				$html .= '
				<tr>
					<td align="left">Капитальный ремонт</td>
					<td align="center">кв.м</td>
					<td align="right">' . $tremont . '</td>
					<td align="right">' . $square . '</td>
					<td align="right">' . $remont . '</td>
				</tr>
				<tr>
					<th align="left" colspan="4">Всего к оплате за расчетный период</th>
					<th align="right">' . $remont . '</th>
				</tr>';			

			$html .= '
			</tbody>
			
		</table><br>	
		Получатель: ' . $receiver . '<br>
		ИНН: ' . $inn . '<br>
		КПП: ' . $kpp . '<br>
		Лицевой счет: ' . $bill . '<br>
		Банк: ' . $bank . '<br>
		Корр. счет: ' . $corraccount . '<br>
		Расчетный счет: ' . $checkaccount . '<br>
		Единый лицевой счет: ' . $unifiedaccount . '<br><br>			
		</td>	
		</tr>
		</table>			  
	</body>
	</html>';
}

use Dompdf\Dompdf;
include_once __DIR__ . '/dompdf/autoload.inc.php';
$dompdf = new Dompdf();
$dompdf->loadHtml($html, 'UTF-8');
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();

// Вывод файла в браузер:
$dompdf->stream('schet-10', array('Attachment'=>0)); 
?>