<!DOCTYPE html>
<html>
<head>
	<title>Квит</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>	
	<style>
	#user {
    width: 250px;
	} 
  </style>
</head>
<meta charset="utf-8">
<body>
	<?php 
	session_start();
	if(isset($_SESSION['userid'])){
		$userid=$_SESSION['userid'];
	}
	else if(isset($_COOKIE['token'])){
		$token=$_COOKIE['token'];		
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");
		$stmt=$mysqli->prepare("SELECT id_user, status FROM users WHERE token=?");
		$stmt->bind_param('i', $token);	
		$stmt->execute();
		$stmt->bind_result($id_user, $statusnew);
		$stmt->fetch();
		$userid=$id_user;
		$status=$statusnew;
		$_SESSION['userid']=$userid;	
		$_SESSION['status']=$status;		
	}
	else{
		header("Location: secure.php");
		exit;
	}
	?>
	
	<?php include ("template/header.php");?>
	<?php include ("template/usermain.php");?>	
	
</body>
</html>