<?php
if(isset($_GET['calcmonth'])&&isset($_GET['calcyear'])){
	$currentmonth=$_GET['calcmonth'];
	$currentyear=$_GET['calcyear'];
	
	$current_year = date ( 'Y' );
	$current_month = date ( 'm' );
	$prev_month=date ( 'm' )-1;
	
	if($currentmonth==1){
		$prevmonth=12;
		$prevyear=$currentyear-1;
	}
	else{
		$prevmonth=$currentmonth-1;
		$prevyear=$currentyear;
	}
	
	// ------------------
	$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
	$mysqli->query("SET NAMES 'utf8';");
	
	$stmt=$mysqli->prepare("DELETE FROM calculation WHERE month=? AND year=?");
	$stmt->bind_param('ii', $currentmonth, $currentyear);	
	$stmt->execute();
	
	$stmt=$mysqli->prepare("SELECT thot, tcold, telec, tgas, tko, remont FROM tariffs");
	$stmt->execute();
	$stmt->bind_result($tariffhot, $tariffcold, $tariffelec, $tariffgas, $tarifftko, $tariffremont);
	while ($stmt->fetch()){
		$thot=$tariffhot;
		$tcold=$tariffcold;
		$telec=$tariffelec;
		$tgas=$tariffgas;
		$tko=$tarifftko;
		$remont=$tariffremont;
	}	
	
	$stmt=$mysqli->prepare("SELECT id_payer, bill, square FROM payer");
	$stmt->execute();
	$stmt->bind_result($id_payer, $bill, $square);
	while ($stmt->fetch()){
		$idpayers[] = "$id_payer";
		$bills[] = "$bill";
		$squares[] = "$square";
	}	
	$num = count($idpayers);
	for($i=0;$i<$num;$i++){		
		
		$check=0;
		$stmt=$mysqli->prepare("SELECT hot, cold, elec, gas FROM meters WHERE id_payer=? AND month=? AND year=?");
		$stmt->bind_param('iii', $idpayers[$i], $prevmonth, $prevyear);	
		$stmt->execute();
		$stmt->bind_result($phot, $pcold, $pelec, $pgas);
		while ($stmt->fetch()){
			$check++;
			$prevhot=$phot;
			$prevcold=$pcold;
			$prevelec=$pelec;
			$prevgas=$pgas;
		}	
		
		$stmt=$mysqli->prepare("SELECT hot, cold, elec, gas FROM meters WHERE id_payer=? AND month=? AND year=?");
		$stmt->bind_param('iii', $idpayers[$i], $currentmonth, $currentyear);	
		$stmt->execute();
		$stmt->bind_result($nowhot, $nowcold, $nowelec, $nowgas);
		while ($stmt->fetch()){
			$check++;
			$currenthot=$nowhot;
			$currentcold=$nowcold;
			$currentelec=$nowelec;
			$currentgas=$nowgas;
		}		
		
		if($check==2){
			
			$chot=$currenthot-$prevhot;
			$hotcost=$chot*$thot;
			
			$ccold=$currentcold-$prevcold;
			$coldcost=$ccold*$tcold;
			
			$celec=$currentelec-$prevelec;
			$eleccost=$celec*$telec;
			
			$cgas=$currentgas-$prevgas;
			$gascost=$cgas*$tgas;			
			
			$ctko=$tko*$squares[$i];
			$cremont=$remont*$squares[$i];
			
			$sum=$hotcost+$coldcost+$eleccost+$gascost+$ctko+$cremont;
			
			$stmt=$mysqli->prepare("INSERT INTO calculation (id_payer, cbill, month, year, chot, hotcost, ccold, coldcost, celec, eleccost, cgas, gascost, ctko, cremont, sum) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param('iiiiisisisissss', $idpayers[$i], $bills[$i], $currentmonth, $currentyear, $chot, $hotcost, $ccold, $coldcost, $celec, $eleccost, $cgas, $gascost, $ctko, $cremont, $sum);	
			$stmt->execute();			
			
		}		
		//echo "$check $prevhot $prevcold $prevelec $prevgas<br>$currenthot $currentcold $currentelec $currentgas<br><br>";		
	}
	// ------------------
	$mysqli-> close();
}
header("Location: calculation.php");
exit;
?>
