<?php
if(isset($_GET['calcmonth'])&&isset($_GET['calcyear'])){
	$currentmonth=$_GET['calcmonth'];
	$currentyear=$_GET['calcyear'];
	
	$current_year = date ( 'Y' );
	$current_month = date ( 'm' );
	$current_day= date ( 'j' );
	
	$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
	$mysqli->query("SET NAMES 'utf8';");
	
	$stmt=$mysqli->prepare("SELECT id_payer, debt, penalty FROM calculation WHERE month=? AND year=?");
	$stmt->bind_param('ii', $currentmonth, $currentyear);
	$stmt->execute();
	$stmt->bind_result($id_payer, $debt, $cpenalty);
	while ($stmt->fetch()){
		$idpayers[] = "$id_payer";
		$debts[] = "$debt";
		$penalties[]="$cpenalty";
	}	
	$num = count($idpayers);
	
	for($i=0;$i<$num;$i++){				
		if($currentyear==$current_year){
			$days=30*($current_month-2-$currentmonth)+$current_day;
		}
		else{			
			$days=30*(11-$currentmonth);			
			$days= 30*($current_month-1)+$current_day+$days;			
		}		
		
		$penalty=$days*$debts[$i]*0.0775/300;
		
		if(round($penalty, 2)!=round($penalties[$i], 2))
			$penalty=$days*$debts[$i]*0.0775/300+$penalties[$i];		
		
		$stmt=$mysqli->prepare("UPDATE calculation SET penalty=? WHERE id_payer=? AND month=? AND year=?");
		$stmt->bind_param('siii', round($penalty, 2), $idpayers[$i], $currentmonth, $currentyear);	
		$stmt->execute();		
	}
	$mysqli-> close();
}
header("Location: calculation.php");
exit;
?>