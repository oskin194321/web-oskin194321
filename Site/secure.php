<?php
	session_start();
	if(isset($_SESSION['userid'])){
		$userid=$_SESSION['userid'];
	}
	else if(isset($_COOKIE['token'])){
		$token=$_COOKIE['token'];				
		LoginByToken($token);
	}
	else if(isset($_POST['login'])&&isset($_POST['pwd'])){
		$login=$_POST['login'];
		$pwd=$_POST['pwd'];		
		LoginByPwd($login, $pwd);
	}
	else{
		header("Location: login.php");
		exit;
	}
	
	function LoginByPwd($login, $pwd)
	{
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");
		$stmt=$mysqli->prepare("SELECT id_user, login, pwdhash, rnd FROM users WHERE login=?");
		$stmt->bind_param('s', $login);	
		$stmt->execute();
		$stmt->bind_result($id_user, $login1, $pwdhash1, $rnd);
		$stmt->store_result();
		if($stmt->num_rows==1){
			$stmt->fetch();
			$pwdhash=sha1($pwd.$rnd);
			if($pwdhash==$pwdhash1){
				$userid=$id_user;
				Login($userid);
			}
			else{
				header("Location: login.php");
				exit;
			} 
		}
		else{
			header("Location: login.php");
			exit;
		} 
	}
	
	function LoginByToken($token)
	{
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");
		$stmt=$mysqli->prepare("SELECT id_user FROM users WHERE token=?");
		$stmt->bind_param('i', $token);	
		$stmt->execute();
		$stmt->bind_result($id_user);
		$stmt->store_result();
		if($stmt->num_rows==1){
			$stmt->fetch();
			$userid=$stmt->id_user;
			Login($userid);
		}
	}
	
	function Login($userid)
	{
		$_SESSION['userid']=$userid;
		$token=rand();
		Setcookie('token', $token, time()+36000);
		$mysqli=mysqli_connect("localhost", "root", "", "house")or die('Connection error.');
		$mysqli->query("SET NAMES 'utf8';");	
		$stmt=$mysqli->prepare("UPDATE users SET token=? WHERE id_user=?");
		$stmt->bind_param('ii', $token, $userid);	
		$stmt->execute();		
		
		$stmt=$mysqli->prepare("SELECT status FROM users WHERE id_user=?");
		$stmt->bind_param('i', $userid);	
		$stmt->execute();
		$stmt->bind_result($statusnew);
		$stmt->store_result();
		if($stmt->num_rows==1){
			$stmt->fetch();
			$status=$statusnew;
			$_SESSION['status']=$status;
		}
		header("Location: index.php");
		exit;
	}
?>